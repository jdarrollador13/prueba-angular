import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsultaAPiService {

  constructor(private http: HttpClient) { }

  getData(url,data){
  	let body:object|any
  	body = Object.assign({}, data)
    //return this.http.get(url);
    return this.http.post(url, body);
  }
}
