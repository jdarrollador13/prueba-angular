import { TestBed } from '@angular/core/testing';

import { ConsultaAPiService } from './consulta-api.service';

describe('ConsultaAPiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsultaAPiService = TestBed.get(ConsultaAPiService);
    expect(service).toBeTruthy();
  });
});
