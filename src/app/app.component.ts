import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ConsultaAPiService } from "./servicio/consulta-api.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'prueba-angular';
  public formOrden:FormGroup;
  public submitted:boolean = false;
  private URL = 'http://127.0.0.1:4000/api/v1/'
  public respuesta:object|any
  private parametros:Array<any> = [];
  public inputResultado:boolean = false
  public inputValue:string|any
  public valor:string|any = ''
  private ejecutar:boolean = false

  constructor(
  	private formBuilder: FormBuilder,	
  	private consultaAPiService:ConsultaAPiService
  ){
  	this.formOrden = this.formBuilder.group({
      infomacion: ['', Validators.required]
    });
  }
  get f() {
    return this.formOrden.controls;
  }
  limpiarFormulario(){
    this.inputResultado = false
    this.respuesta = ''
    this.valor = ''
    this.formOrden.reset()
  }
  consultarInformacion($event) {
  	if (this.formOrden.invalid) {
      this.submitted = true;
      return;
    }
    this.ejecutar = false
    this.respuesta = ''
    this.valor = ''
    let obtenerData:string|any = this.formOrden.value.infomacion;
    //console.log(obtenerData.trim().replace(/ /g, "").split(','))
    let convertirArray:[]|any = obtenerData.trim().replace(/ /g, "").split(',')
    let arrayBase = ['Nombre','Apellido','Ciudad','Direccion','Telefono']
    for( let i = 0; i < convertirArray.length; i++ ){
      for( let x = 0; x < arrayBase.length; x++ ){
        if(convertirArray[i] == arrayBase[x]){
          this.ejecutar = true
         let key:string|any = convertirArray[i]
         this.parametros[key] = convertirArray[i]
        }
      }
    }
    if( this.ejecutar == false ){
      this.inputResultado = false
      console.log('ingreso')
      return
    }else{
      console.log('paso')
      this.consultaAPiService.getData(`${this.URL}listar/personas`,this.parametros).subscribe((data: object|any) => {
        this.formOrden.reset()
        this.parametros = []
        let key:string|any
        this.inputResultado = true  
        this.respuesta = data.data[0]
        for( let x  in this.respuesta ){
          if(x != 'IdPersonal'){
            key = this.respuesta[x]
            this.valor+=`${key},`
          }
        }
      })
    }
  }
}
